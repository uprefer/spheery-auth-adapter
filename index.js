
function validateAuthData(authData, options) {
    if(!Object.keys(authData).includes("spheery")){
        throw new Parse.Error(Parse.Error.OBJECT_NOT_FOUND, 'No adapter found for ', Object.keys(authData));
    }

    if(!authData.spheery.id){
        throw new Parse.Error(Parse.Error.OBJECT_NOT_FOUND, 'No data found for ', authData);
    }
    console.log(authData);
    return;
}

function validateAppId(){ return Promise.resolve();}

module.exports = {
    validateAuthData: validateAuthData,
    validateAppId: validateAppId
}